def prodZ(x,y):     # Funktionsdeklaration
    i = 0           # Initialisierungen
    z = 0
    if (x < 0):   
        x = (0 - x)         # neg. Vorz. von x entfernen
        y = (0 - y)         # und auf y uebertragen
    for i in range(0,x):    # x Durchlaeufe
            z = (z + y)     # y wird x-mal zu z addiert
    return z

def fakul1(x):
    z = 1
    if(x>0):
        for i in range(0,x):
            z=prodZ(z,x)
            x=(x-1)
    else:
        z=0
    return z

def fakul2(x):
    if (x>1):
        x=prodZ(x,fakul2((x-1)))
    return x

def fakul3(x):
    z=0
    if(x>0):
        z=1
        y=1
        while(y<x):
            y=(y+1)
            z=prodZ(y,z)
    return z
