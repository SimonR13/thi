def prodZ(x,y):     # Funktionsdeklaration
    i = 0           # Initialisierungen
    z = 0
    if (x < 0):   
        x = (0 - x)         # neg. Vorz. von x entfernen
        y = (0 - y)         # und auf y uebertragen
    for i in range(0,x):    # x Durchlaeufe
            z = (z + y)     # y wird x-mal zu z addiert
    return z

def fakul3(x):
    z=0
    if(x>0):
        z=1
        y=1
        while(y<x):
            y=(y+1)
            z=prodZ(y,z)
    return z
