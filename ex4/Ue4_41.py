def prodZ(x,y):     # Funktionsdeklaration
    i = 0           # Initialisierungen
    z = 0
    if (x < 0):   
        x = (0 - x)         # neg. Vorz. von x entfernen
        y = (0 - y)         # und auf y uebertragen
    for i in range(0,x):    # x Durchlaeufe
            z = (z + y)     # y wird x-mal zu z addiert
    return z

def fakul1(x):
    z = 1
    if(x>0):
        for i in range(0,x):
            z=prodZ(z,x)
            x=(x-1)
    else:
        z=0
    return z

