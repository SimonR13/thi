def sollwert(n,x):
    if g(n) == x:
        return "TRUE"
    else:
        return "FALSE"

def g(n):
    if n <= 1:
        return 1
    if n > 1:
        return (g(n-1) + pow(n,3))
