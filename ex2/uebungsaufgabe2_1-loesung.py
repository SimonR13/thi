def sollwert(n):
    if n == 1:
        return 1
    else:
        return ((n*(n+1)//2)**2

def g(n):
    if n == 1:
        return 1
    if n > 1:
        return (g(n-1) + pow(n,3))
