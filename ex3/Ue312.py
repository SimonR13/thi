def prefix_set(w):
	a=0
	menge=set()
	while a<=len(w):
		menge.add(w[0:a])
		a+=1
	return menge

def suffix_set(w):
        a=len(w)
        menge = set()
        while a>=0:
                menge.add(w[a:])
                a-=1
        return menge
