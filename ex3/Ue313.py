def concat(L1, L2):
    L=set()
    for w in L1:
        for v in L2:
            L.add(w + v)
    return L

def concat_k(L,k):
    if k == 0:
        return ('')
    else:
        return concat(concat_k(L,k-1),L)
     

