def cM(x):
    sum1=prodZ(2,x)
    result = 0
    for i in range(0,(sum1+1)):
        prim1 = prim(i)
        for j in range(0,(sum1+1)):
            prim2 = prim(j)
            sum2=(prim1+prim2)
            if(sum2 == sum1):
                result = 1
    return result

def prim(n):        #n-te primzahl
    p=2
    if(n>=1):
        while(n>=0):
            if(teil(p)==2):
                n=(n-1)
            p=(p+1)
        p=(p-1)
    return p

def modZ(x,y):
    if (y!=0):
        z=(x-prodZ(y,divZ(x,y)))
    return z

def divZ(x,y):
    if (y != 0):    # nicht def. Rueckgabe, falls y=0
        if (y < 0):
            y = (0 - y)     # neg. Vorzeichen von y
            x = (0 - x)     # auf x uebertragen
        z = 0
        if (x < 0):         # z = min(0,x)
            z = x
                            # hier gilt y>0 und y*z <= x
        while (prodZ(y,z) <= x):    # suche groesstes z 
            z = (z + 1)             # mit y*z<=x
        z = (z - 1)         # zu weit gezaehlt
    return z

def prodZ(x,y):     # Funktionsdeklaration
    i = 0           # Initialisierungen
    z = 0
    if (x < 0):   
        x = (0 - x)         # neg. Vorz. von x entfernen
        y = (0 - y)         # und auf y uebertragen
    for i in range(0,x):    # x Durchlaeufe
            z = (z + y)     # y wird x-mal zu z addiert
    return z

def teil(x):
    z=0
    c=0
    if (x>=1):
        while(c<x):
            c=(c+1)
            if(modZ(x,c)==0):
                z=(z+1)
    if (x<1):
        z=0
    return z
